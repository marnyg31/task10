﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task10
{
    class UserIO
    {

        public static int ReadIntFromUser(string prompt)
        {
            //method to handle int reads from user
            Console.WriteLine(prompt);
            bool succesfulRead = false;
            int input = 0;
            while (!succesfulRead)
            {

                if (int.TryParse(Console.ReadLine(), out input))
                {
                    succesfulRead = true;
                }
                else
                {
                    Console.WriteLine("Error reading input, please enter another int");
                }
            }
            return input;
        }
        public static double ReadDoubleFromUser(string prompt)
        {
            //method to handle double reads from user
            Console.WriteLine(prompt);
            bool succesfulRead = false;
            double input = 0;
            while (!succesfulRead)
            {
                if (double.TryParse(Console.ReadLine(), out input))
                {
                    succesfulRead = true;
                }
                else
                {
                    Console.WriteLine("Error reading input, please enter another double");
                }
            }
            return input;
        }
        public static string ReadStringFromUser(string prompt)
        {
            //method to handle string reads from user
            Console.WriteLine(prompt);
            bool succesfulRead = false;
            string input = "";
            while (!succesfulRead)
            {
                input = Console.ReadLine();
                if (input.Length > 0)
                {
                    succesfulRead = true;
                }
                else
                {
                    Console.WriteLine("Error reading input, please enter another string");
                }
            }
            return input;
        }

        public static string GetSelectionFromUser(List<string> options)
        {
            //method to handle user input when we want selection from a list of options
            string str = ReadStringFromUser("pleas select one");
            while (!options.Contains(str)) str = ReadStringFromUser("pleas select one");
            return str;
        }

        public static string PromptForAnimalCreationOrUsePreset(List<string> selectionOptions)
        {
            //ask user if he wants a preset or animals, or if he wants to create his own
            Console.WriteLine("would you like to use a preset of animals, or creat your own");
            Console.WriteLine("(p) to use preset, (c) to create your own");
            string selection = GetSelectionFromUser(selectionOptions);
            Console.Clear();
            return selection;
        }

        public static string PromptForFilteringOptions(List<string> selectionOptions)
        {
            //ask user what he would like to do to the collection of animals
            Console.WriteLine("which property do you want to filter across");
            Console.WriteLine("(n) name, (h) height, (l) leg number,(p) print all, (q) quit");
            string selection = GetSelectionFromUser(selectionOptions);
            Console.Clear();
            return selection;
        }

        public static bool HandleUserSelection(List<Animal> animals, string selectedFilteringProperty)
        {
            //call appropriate function for each given user selection
            IEnumerable<Animal> filteredAnimals=null;

            if (selectedFilteringProperty == "q") return true;
            else if (selectedFilteringProperty == "p") AnimalFilerer.PrintAnimals(animals);
            else if (selectedFilteringProperty == "n") filteredAnimals = AnimalFilerer.FilterForName(animals); 
            else if (selectedFilteringProperty == "h") filteredAnimals = AnimalFilerer.FilterForHeight(animals); 
            else if (selectedFilteringProperty == "l") filteredAnimals = AnimalFilerer.FilterForLegs(animals);
            AnimalFilerer.PrintAnimals(filteredAnimals);
            return false;
        }
    }
}
