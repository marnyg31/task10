﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task10
{
    abstract class Bird : Animal
    {
        protected Bird(string name, double heightInMeters, int numLegs) : base(name, heightInMeters, numLegs)
        {
            //User base constructor
        }
        protected Bird(string name, int numLegs) : base(name, numLegs)
        {
            //User base constructor
        }

        //Define abstract method all children need to override
        public abstract void fly();

        public override void Act()
        {
            //Use act to call fly, so that i can use Act() on all animals and get an expected result
            this.fly();
        }

    }
}
