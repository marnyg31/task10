﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task10
{
    interface IWalker
    {
        //method to be implemented in classes that use this interface
        public void WalkByInterface();
    }
}
