﻿using System;
using System.Collections.Generic;
using System.Text;
using Task10;

namespace Task10
{
    class GlidingBird : Bird, IFlyer, IWalker
    {
        public GlidingBird(string name, double heightInMeters, int numLegs) : base(name, heightInMeters, numLegs)
        {
            //User base constructor
        }
        public GlidingBird(string name, int numLegs) : base(name, numLegs)
        {
            //User base constructor
        }

        public override void fly()
        {
            //Print the action of the animal type
            Console.WriteLine(this.Name + " is gliding with " + this.NumLegs + " legs");
        }
        public void FlyByInterface()
        {
            //Implementing method requierd by interface
            Console.WriteLine(this.Name + " is flapping it's wings and flying with all the IFlyers");
        }

        public void WalkByInterface()
        {
            //Implementing method requierd by interface
            Console.WriteLine(this.Name + " is a bird walking with all the IWalkers");
        }
    }
}
