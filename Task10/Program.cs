﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;

namespace Task10
{
    class Program
    {
        static void Main(string[] args)
        {
            //Ask if user want to use a preset of animals, or create their own
            string selection = UserIO.PromptForAnimalCreationOrUsePreset(new List<string>() { "p", "c" });
            List<Animal> animals = AnimalCreator.CreateAnimalsBasedOnSelection(selection);

            bool done = false;
            while (!done)
            {
                //Main loop
                //Ask the user what he wants to do with the collection of animals
                string selectedFilteringProperty = UserIO.PromptForFilteringOptions(new List<string>() { "n", "h", "l", "p", "q" });
                //Execute the function the user selected
                done = UserIO.HandleUserSelection(animals, selectedFilteringProperty);
            }
        }
    }
}
