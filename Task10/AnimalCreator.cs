﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task10
{
    class AnimalCreator
    {
        public static List<Animal> UsePresetAnimals()
        {
            //return a preset list of animals
            return new List<Animal>()
            {
                new Snake("Snek",0.1,0),
                new Pig("Piggy",0.5,4),
                new GlidingBird("Snek",0.2,2),
                new FlyingBird("Snek",0.2,2),
                new Pig("Stumpy",0.6,3)
            };
        }

        public static List<Animal> CreateAnimals()
        {
            //guid user trough animal creation, until he has created at least 5 animals
            List<Animal> animals = new List<Animal>();
            bool done = false;
            while (!done)
            {
                Console.Clear();
                Console.WriteLine($"you have created {animals.Count}, you need at least 5");
                Console.WriteLine("which animal: (p) pig, (s) snake, (f) flying bird, (g) gliding bird, (d) done");
                string selection = UserIO.GetSelectionFromUser(new List<string>() { "p", "s", "f", "g", "d" });
                if (selection == "d" && animals.Count > 4) done = true;
                else if (selection == "d") Console.WriteLine("you need to create at least 5 animals");
                else animals.Add(CreateAnimalFromSelection(selection));
            }
            Console.Clear();
            return animals;
        }
        private static Animal CreateAnimalFromSelection(string selection)
        {
            //read data from user about the animal he want to create
            //then create the type of animal the user selected
            Animal animal = new Animal("defaultTommy", 1.1, 4);
            string name = UserIO.ReadStringFromUser("enter name of animal");
            double heigt = UserIO.ReadDoubleFromUser("enter height of animal");
            int numLegs = UserIO.ReadIntFromUser("enter number of legs the animal has");
            switch (selection)
            {
                case "p":
                    animal = new Pig(name, heigt, numLegs);
                    break;
                case "s":
                    animal = new Snake(name, heigt, numLegs);
                    break;
                case "f":
                    animal = new FlyingBird(name, heigt, numLegs);
                    break;
                case "g":
                    animal = new GlidingBird(name, heigt, numLegs);
                    break;
            }
            return animal;
        }

        public static List<Animal> CreateAnimalsBasedOnSelection(string selection)
        {
            //check if user want to use a preset of animals, or create his own
            if (selection == "c") return AnimalCreator.CreateAnimals();
            else return AnimalCreator.UsePresetAnimals();
        }
    }
}
