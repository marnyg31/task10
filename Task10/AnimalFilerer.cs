﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Task10
{
    class AnimalFilerer
    {
        public static void PrintAnimals(IEnumerable<Animal> filteredAnimals)
        {
            //print a list of animals
            if (filteredAnimals == null) return;
            foreach (var animal in filteredAnimals) Console.WriteLine(animal);
            Console.WriteLine();
        }

        public static IEnumerable<Animal> FilterForLegs(List<Animal> allAnimals)
        {
            //filter a list of animals based on number of legs
            int numLegs = UserIO.ReadIntFromUser("enter number of legs to filter for:");
            return from animal in allAnimals where animal.NumLegs== numLegs select animal;
            // return allAnimals.Where(animal => animal.NumLegs == numLegs);
        }

        public static IEnumerable<Animal> FilterForHeight(List<Animal> allAnimals)
        {
            //filter a list of animals based on height
            double approximateHeight = UserIO.ReadDoubleFromUser("enter approximate height of animal to filter for");
            return from animal in allAnimals where (Math.Abs(animal.HeightInMeters - approximateHeight) < 0.1) select animal;
            // return allAnimals.Where(animal => Math.Abs(animal.HeightInMeters - approximateHeight) < 0.1);
        }

        public static IEnumerable<Animal> FilterForName(List<Animal> allAnimals)
        {
            //filter a list of animals based on name
            string filterString = UserIO.ReadStringFromUser("enter string to filter names:");
            return from animal in allAnimals where animal.Name.Contains(filterString) select animal;
            // return allAnimals.Where(animal => animal.Name.Contains(filterString));
        }
    }
}
