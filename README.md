Solution for task 10

Upon running the code, the user will be prompted to select wether or not he wants to work with a preset collection of animals, or if he would like to create his own.

If he chooses to create his own, he will be prompted for an animal type to create, then he will be      asked to input varios info about the animal. He will need to create at least 5 animals before he can continue.

Then the user will be presented with the filtering menu, where he can choose which attribute of the   animal he would like to filter with. then the user is asked to enter the fileting quiteria and a       filtered list will be printed to the console.

The relevant code is found in /task10/Program.cs, /task10/AnimalCreator.cs, /task10/AnimalFilterer.cs, /task10/UserIO.cs. All animal classes are found in /task10/Animals/

